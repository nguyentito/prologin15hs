import Control.Applicative
import Control.Monad
import Data.List

main :: IO ()
main = join $ creneaux <$> (read <$> getLine) <*> (read <$> getLine)

creneaux :: Int -> Int -> IO ()
creneaux n h = do
  replicateM_ n (putStr " _")
  putStr "\n"
  let line = intercalate " " (["|"] ++ replicate (n-1) "|_|" ++ ["|"])
      len = length line
  putStrLn line
  replicateM_ (h-2) . putStrLn $ "|" ++ replicate (len-2) ' ' ++ "|"
  putStrLn $ "|" ++ replicate (len-2) '_' ++ "|"
