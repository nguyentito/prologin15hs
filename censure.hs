import Control.Applicative

main :: IO ()
main = do
  n <- read <$> getLine
  str <- take n <$> getLine
  m <- read <$> getLine
  positions <- take m . map read . words <$> getLine
  censure m str n positions

censure :: Int -> String -> Int -> [Int] -> IO ()
censure n str m positions = putStrLn . unwords . process 1 positions . words $ str
  where process k (p:ps) (w:ws) | k == p = map (const '*') w : process (k+1) ps ws
                                | otherwise = w : process (k+1) (p:ps) ws
        process _ [] ws = ws

