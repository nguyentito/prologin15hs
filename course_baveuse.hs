import Control.Applicative
import Control.Monad
import Data.Function
import Data.List

main :: IO ()
main = do
  [n, c] <- map read . words <$> getLine
  joueur1 <- replicateM n $ do
    [p, v] <- map read . words <$> getLine
    pure (p, v)
  joueur2 <- replicateM n $ do
    [p, v] <- map read . words <$> getLine
    pure (p, v)
  courseBaveuse n c joueur1 joueur2

courseBaveuse :: Int -> Int -> [(Int, Int)] -> [(Int, Int)] -> IO ()
courseBaveuse n c joueur1 joueur2 = print $ f j1 j2
  where j1 = zip [1..] joueur1
        j2 = zip [(n+1)..] joueur2
        f (e1@(_,(p1,v1)):e1s) (e2@(_,(p2,v2)):e2s)
          | p1 * v1 <= p2 * v2 = -- escargot 2 gagne le duel
              f e1s (e2:e2s)
          | otherwise = f (e1:e1s) e2s
        f e1s [] = fst . maximumBy (compare `on` (snd.snd)) $ e1s
        f [] e2s = fst . maximumBy (compare `on` (snd.snd)) $ e2s

