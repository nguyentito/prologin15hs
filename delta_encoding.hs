import Control.Applicative
import Control.Monad

main :: IO ()
main = join $ deltaEncoding <$> (read <$> getLine) <*> (map read . words <$> getLine)

deltaEncoding :: Int -> [Int] -> IO ()
deltaEncoding _ (x:xs) = putStrLn . unwords . map show $ x : zipWith (-) xs (x:xs)

