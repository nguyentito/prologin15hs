import Control.Applicative
import Control.Monad

main :: IO ()
main = do
  n <- read <$> getLine
  tab <- forM [1..n] $ \i -> map read . take i . words <$> getLine
  ecoulement n tab

ecoulement :: Int -> [[Int]] -> IO ()
ecoulement n tab = print . f  $ tab
  where f ((x:xs):ys:q) =
          f (zipWith (+) ys ([x] ++ zipWith min (x:xs) xs ++ [last (x:xs)]) : q)
        f [xs] = maximum xs
