import Control.Applicative
import Control.Monad
import Data.Function
import Data.List
import Data.Ratio

courseElephants :: Int -> Int -> [Int] -> [Int] -> Int
courseElephants n l vs ps =
  fst . minimumBy (compare `on` snd)
  $ zip [0..] (zipWith (%) (map ((-) l) ps) vs)


main :: IO ()
main = print =<< courseElephants <$> (read <$> getLine)
                                 <*> (read <$> getLine)
                                 <*> (map read . words <$> getLine)
                                 <*> (map read . words <$> getLine)
  
